//: Playground - noun: a place where people can play

//https://stavash.wordpress.com/2012/12/14/advanced-issues-asynchronous-uitableviewcell-content-loading-done-right/

import UIKit

class FacebookFriend {

    var friendID: String
    var name: String
    var imageURL: String

    init(friendID: String, name: String, imageURL: String) {
        self.friendID = friendID
        self.name = name
        self.imageURL = imageURL
    }
}

class FacebookFriedCell: UITableViewCell {

    var lblName: UILabel = UILabel()
    var ivProfile: UIImageView = UIImageView()
}

class UIFriendsViewController: UITableViewController {

    let cellID = "cell"

    var facebookUidToImageDownloadOperations: [String : BlockOperation] = [:]
    let imageLoadingOperationQueue = OperationQueue()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(FacebookFriedCell.self, forCellReuseIdentifier: cellID)
    }

    let facebookFriends : [FacebookFriend] = [
        FacebookFriend(friendID: "1", name: "Iron man", imageURL: "https://images-na.ssl-images-amazon.com/images/I/515wjJQt2nL._SY445_.jpg"),
        FacebookFriend(friendID: "2", name: "Hulk", imageURL: "https://lumiere-a.akamaihd.net/v1/images/tmb-sq_character-hulk_launch_8ce79435.jpeg"),
        FacebookFriend(friendID: "3", name: "Hulk", imageURL: "https://cdn.newsapi.com.au/image/v1/28811c194076a2f2730455884bdf29ae")]


    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return facebookFriends.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let friend = facebookFriends[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! FacebookFriedCell
        cell.lblName.text = friend.name

        //Create a block operation for loading the image into the profile image view
        let loadImageIntoCellOp = BlockOperation()
        weak var weakOp = loadImageIntoCellOp

        loadImageIntoCellOp.addExecutionBlock {
            //Some asynchronous work. Once the image is ready, it will load into view on the main queue
            if let url = URL(string: friend.imageURL) {
                if let data = try? Data(contentsOf: url) {
                    if let profileImage = UIImage(data: data) {
                        OperationQueue.main.addOperation {
                            //Check for cancelation before proceeding. We use cellForRowAtIndexPath to make sure we get nil for a non-visible cell
                            if (weakOp != nil && !weakOp!.isCancelled) {
                                let cell = tableView.dequeueReusableCell(withIdentifier: self.cellID, for: indexPath) as! FacebookFriedCell
                                cell.ivProfile.image = profileImage
                                self.facebookUidToImageDownloadOperations[friend.friendID] = nil
                            }
                        }
                    }
                }
            }
        }
        //Save a reference to the operation in an NSMutableDictionary so that it can be cancelled later on
        if !friend.friendID.isEmpty {
            facebookUidToImageDownloadOperations[friend.friendID] = loadImageIntoCellOp
        }

        //Add the operation to the designated background queue
        imageLoadingOperationQueue.addOperation(loadImageIntoCellOp)

        //Make sure cell doesn't contain any traces of data from reuse -
        //This would be a good place to assign a placeholder image
        cell.ivProfile.image = nil
        return cell
    }

    //It’s called right after the cell we are loading our data into is no longer needed
    override func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        let friend = facebookFriends[indexPath.row]
        //Fetch operation that doesn't need executing anymore
        if let ongoingDownloadOperation = facebookUidToImageDownloadOperations[friend.friendID] {
            ongoingDownloadOperation.cancel()
            facebookUidToImageDownloadOperations[friend.friendID] = nil
        }
    }

    //don’t forget to take advantage of the NSOperationQueue and call “cancelAllOperations” when the table is not needed anymore
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        imageLoadingOperationQueue.cancelAllOperations()
    }
}










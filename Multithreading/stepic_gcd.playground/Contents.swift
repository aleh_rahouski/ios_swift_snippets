//: Playground - noun: a place where people can play

import UIKit

//for dispatch async_after to be able to work
import PlaygroundSupport
PlaygroundPage.current.needsIndefiniteExecution = true

/// QUEUES ///

class QueueTest1 {

    private let serialQueue = DispatchQueue(label: "serialTest")
    private let concurrentQueue = DispatchQueue(label: "concurrentTest", attributes: .concurrent)

    //you have ability to get queue from pool of queues (global)
    // all global queues are concurrent, except of main - it's serial
    private let globalQueue = DispatchQueue.global()
    //serial global queue - main thread
    private let mainQueue = DispatchQueue.main
    //all global queues are created by system, so frequently they are used for systems tasks
    //it's not reccomended to run long-running tasks on global queues
}

/// Async vs Sync ///

// Async - управление возвращается сразу тому потоку, который вызвал задачу
// sync - поток ждет заверешения задачи и только после этого получает управление

class AsyncVsSyncTest1 {

    private let serialQueue = DispatchQueue(label: "serial")

    func testSerial() {
        serialQueue.async {
            print("test1")
        }
        serialQueue.async {
            sleep(1)
            print("test2")
        }
        serialQueue.sync {
            print("test3")
        }
        serialQueue.sync {
            print("test4")
        }
    }
}

//let test = AsyncVsSyncTest1()
//test.testSerial()

class AsyncVsSyncTest2 {

    private let concurrentQueue = DispatchQueue.global()

    func testConcurrent() {

        concurrentQueue.async {
            print("test1")
        }
        concurrentQueue.async {
            print("test2")
        }
        //гарантируется только то, что задача test3 будет выполнена раньше чем задача test4
        concurrentQueue.sync {
            print("test3")
        }
        concurrentQueue.sync {
            print("test4")
        }
    }
}

//let test2 = AsyncVsSyncTest2()
//test2.testConcurrent()

/// Async After ///

class AsyncAfterTest {

    private let concurrentQueue = DispatchQueue(label: "AsyncAfterTest", attributes: .concurrent)

    func test() {

        print("start")
        concurrentQueue.asyncAfter(deadline: .now() + 3) {
            print("end")
        }
    }
}

//let test3 = AsyncAfterTest()
//test3.test()

// Дан код:

/*let serialQueue = DispatchQueue(label: "serialQueue")
serialQueue.async {
    print("test1")
}
print("test2")*/

//Что выведется на экран? --- Возможны оба варианта

//Дан код:

/*let concurrentQueue = DispatchQueue(label: "concurrentQueue", attributes: .concurrent)
concurrentQueue.sync {
    print("test1")
}
print("test2")*/

//Что выведется на экран? -- test1, test2

/*Дан код:

serialQueue.sync {
    print("test1")
}
serialQueue.async {
    print("test2")
}
serialQueue.sync {
    print("test3")
}

Что выведется на экран? test1, test2, test3*/

/// Concurrent Perform

class ConcurrentPerformTest {

    func test() {

        DispatchQueue.concurrentPerform(iterations: 3) { (i) in
            print(i)
        }
    }
}

//let test = ConcurrentPerformTest()
//test.test()

/// Dispatch Work Item - абстракция над задачей, которую нужно выполнить - альтернатива блоку

class DispatchWorkItemTest {

    private let queue = DispatchQueue(label: "DispatchWorkItemTest", attributes: .concurrent)

    func testNotify() {

        let item = DispatchWorkItem {
            print("code inside DispatchWorkItem")
        }
        item.notify(queue: queue) {
            print("finish DispatchWorkItem")
        }
        queue.async(execute: item)
    }
}

//let itemTest = DispatchWorkItemTest()
//itemTest.testNotify()

class DispatchWorkItemTest2 {

    private let queue = DispatchQueue(label: "DispatchWorkItemTest2")

    func testCancel() {

        queue.async {
            sleep(1)
            print("test1")
        }
        queue.async {
            sleep(1)
            print("test2")
        }

        let item = DispatchWorkItem {
            print("item test")
        }
        //отменить выполнение задачи можно только до того момента, как она поставлена на выполнение
        queue.async(execute: item)
        item.cancel()
    }
}

//let itemtest2 = DispatchWorkItemTest2()
//itemtest2.testCancel()

/*let item = DispatchWorkItem {
    print("test")
}
let serialQueue = DispatchQueue(label: "serialQueue")
serialQueue.async(execute: item)
sleep(1)
item.cancel()

Что выведется на экран? - test*/

/// === Дан код:===

/*let serialQueue = DispatchQueue(label: "serialQueue")
let item = DispatchWorkItem {
    print("test")
}
item.notify(queue: serialQueue) {
    print("finish")
}
serialQueue.async {
    sleep(1)
}
serialQueue.async(execute: item)
item.cancel()*/

// Что выведется на экран? - finish

//Подсказка: notify - команда, которая вызывается по окончанию работы Work Item. Окончание работы не всегда может быть успешным, но тем не менее, оно произойдёт в любом случае, если Work Item закинут в очередь.

/// Semaphore ///

class SemaphoreTest {

    //value - регулирует количество потоков, которые могут одновременно обращаться к ресурсу
    private let semaphore = DispatchSemaphore(value: 0)

    func test() {

        DispatchQueue.global().async {

            sleep(3)
            print("1")
            self.semaphore.signal()
        }
        //вызывающий поток (в данном случае - главный поток) блокируется до тех пор, пока не
        //будет вызван semaphore.signal()
        semaphore.wait()
        print("2")
    }
}

//let semaphoreTest = SemaphoreTest()
//semaphoreTest.test()

class SemaphoreTest2 {

    // 2 потока могут одновременно обращаться
    private let semaphore = DispatchSemaphore(value: 2)

    func doWork() {

        semaphore.wait()
        print("test")
        sleep(3) // do something
        semaphore.signal()
    }

    func test() {

        //1 и 2 распечатают test одновременно
        DispatchQueue.global().async {
            self.doWork()
        }
        DispatchQueue.global().async {
            self.doWork()
        }
        // 3 приступит к выполнению только когда ресурс осводится и напечатает свой test тоько через 3 секунды
        DispatchQueue.global().async {
            self.doWork()
        }
    }
}

let t = SemaphoreTest2()
t.test
//

/*Дан код:

class SemaphoreTest3 {
        private let semaphore = DispatchSemaphore(value: 1)

            func doWork( ) {
                    semaphore.wait( )
                    print(«test»)
                    sleep(2) //Do something
                    semaphore.signal( )
                }

            func test( ) {
                    DispatchQueue.global ( ).async {
                            self.doWork( )
                        }
                    DispatchQueue.global ( ).async {
                            self.doWork( )
                        }
                    DispatchQueue.global ( ).async {
                            self.doWork( )
                        }
                }
}

Что выведется на экран? test /2сек/ test /2cек/ test */







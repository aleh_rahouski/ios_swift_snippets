//: Playground - noun: a place where people can play

import UIKit

/*var thread1 = pthread_t(bitPattern: 0)
var attr1 = pthread_attr_t()

pthread_attr_init(&attr1)
pthread_create(&thread1, &attr1, { pointer in

    print("test1")

    return nil
}, nil)

var thread2 = pthread_t(bitPattern: 0)
var attr2 = pthread_attr_t()

pthread_attr_init(&attr2)
pthread_create(&thread2, &attr2, { pointer in

    print("test2")

    return nil
}, nil)*/

////

let thread1 = Thread {

    print("test1")
    let thread2 = Thread {
        print("test2")
    }
    thread2.start()
}

thread1.start()

///// pthread qos

class PthreadQosTest {

    func test() {
        var thread = pthread_t(bitPattern: 0)
        var attribute = pthread_attr_t()
        pthread_attr_init(&attribute)
        pthread_attr_set_qos_class_np(&attribute, QOS_CLASS_USER_INITIATED, 0)
        pthread_create(&thread, &attribute, { (pointer) -> UnsafeMutableRawPointer? in

            print("test")
            pthread_set_qos_class_self_np(QOS_CLASS_BACKGROUND, 0)

            return nil
        }, nil)
    }
}

class QoSThreadTest {

    func test() {

        let thread = Thread {
            print("QoSThreadTest: test")
            print("QoSThreadTest: \(qos_class_self())")
        }
        thread.qualityOfService = .background
        thread.start()

        print("Main thread: \(qos_class_main())")
    }
}

let qoSThreadTest = QoSThreadTest()
qoSThreadTest.test()

///// Что выведется на экран? - Возможны оба варианта
let thread11 = Thread {
    print("test11")
}
thread11.qualityOfService = .utility
thread11.start()

let thread12 = Thread {
    print("test12")
}
thread12.qualityOfService = .userInitiated
thread12.start()
//


//// MUTEX ////

class MutexTest {

    private var mutex = pthread_mutex_t()

    init() {
        pthread_mutex_init(&mutex, nil)
    }

    func test() {
        pthread_mutex_lock(&mutex)
        //do something
        pthread_mutex_unlock(&mutex)
    }
}

public class NSLockTest {

    private let lock = NSLock()

    func test(i: Int) {
        lock.lock()
        //do something
        lock.unlock()
    }
}

class RecursiveMutexTest {

    private var mutex = pthread_mutex_t()
    private var attrs = pthread_mutexattr_t()

    init() {
        pthread_mutexattr_init(&attrs)
        pthread_mutexattr_settype(&attrs, PTHREAD_MUTEX_RECURSIVE)
        pthread_mutex_init(&mutex, &attrs)
    }

    func test1() {
        pthread_mutex_lock(&mutex)
        test2()
        pthread_mutex_unlock(&mutex)
    }

    func test2() {
        pthread_mutex_lock(&mutex)
        //do something
        pthread_mutex_unlock(&mutex)
    }
}

//В каких случаях стоит применить Recursive Lock?
// - чтобы разрешить потоку захватывать один и тот же ресурс несколько раз

class RecursiveLockTest {

    private let lock = NSRecursiveLock()

    private func test1() {
        lock.lock()
        test2()
        lock.unlock()
    }

    private func test2() {
        lock.lock()
        //do something
        lock.unlock()
    }
}

//conditions - ключевая особенность conditions - задача, закрытая condition, не начнет
//выполняться до того момента, пока не получит некий сигнал из другого потока

//pthread_cond

class MutexConditionTest {

    private var condition = pthread_cond_t()
    private var mutex = pthread_mutex_t()
    private var check = false

    init() {
        pthread_cond_init(&condition, nil)
        pthread_mutex_init(&mutex, nil)
    }

    func test1() {
        // 1 - захват ресурса
        pthread_mutex_lock(&mutex)
        // 2 - проверка флага, и если он false, мы ждем
        while !check {
            pthread_cond_wait(&condition, &mutex)
        }
        // 3 - проверка прошла - мы выполняем какое-либо полезное действие
        // 6 - do something
        pthread_mutex_unlock(&mutex)
    }

    func test2() {
        pthread_mutex_lock(&mutex)
        // 4 выставляем флаг в true
        check = true
        // 5 сигнализируем, что condition поменялся
        pthread_cond_signal(&condition)
        pthread_mutex_unlock(&mutex)
    }
}

//NSCondition

class ConditionTest {

    private let condition = NSCondition()
    private var check = false

    func test1() {
        condition.lock()
        while !check {
            condition.wait()
        }
        condition.unlock()
    }

    func test2() {
        condition.lock()
        check = true
        condition.signal()
        condition.unlock()
    }
}

//Что выведется на экран?
// - Возможны оба варианта

/*class Question {
    
    private let condition = NSCondition()
    private var check: Bool = false

    func test_1() {
        condition.lock()
        while(!check) {
            condition.wait()
        }
        print("test")
        condition.unlock()
    }

    func test_2() {
        condition.lock()

        check = true
        condition.unlock()
    }

    let thread1 = Thread {
        test_1()
    }
    thread1.start()

    let thread2 = Thread {
        test_2()
    }
    thread2.start()
}*/

// Read - write locks - блокируют ресурс на запись и не блокируют его на чтение

///// READ-WRITE LOCKS

// only unix variant exists for RWL, Foundation doesn't have it's own implementation
class ReadWriteLockTest {

    private var lock = pthread_rwlock_t()
    private var attrs = pthread_rwlockattr_t()

    //наш ресурс
    private var test: Int = 0

    init() {
        pthread_rwlock_init(&lock, &attrs)
    }

    var testProperty: Int {
        get {
            pthread_rwlock_rdlock(&lock)
            let tmp = test
            pthread_rwlock_unlock(&lock)
            return tmp
        }
        set {
            pthread_rwlock_wrlock(&lock)
            test = newValue
            pthread_rwlock_unlock(&lock)
        }
    }
}

//Spin lock

// по своей сути это цикл while, который постоянно опрашивает, освобожден ресурс или нет
// этот способ очень ресурсозатратен
class SpinLockTest {

    private var lock = OS_SPINLOCK_INIT

    func test() {
        //deprecated: 10.0, message: "Use os_unfair_lock_lock() from <os/lock.h> instead
        OSSpinLockLock(&lock)
        //do something
        OSSpinLockUnlock(&lock)
    }
}

//Unfair lock - начиная с iOS 10
//обычно мьютексы работают по принципу FIFO - если 2 потока, обращаются к ресурсу, закрытому
//мьютексом, ресурс захватывает первый поток, который к нему обратился. после того как первый поток выполнил свою задачу, и освободил ресурс, его захватывает другой поток.

// в случае с UnfairLock - другая ситуация
// при переключении между потоками происходит context switch - довольно дорогостоящая операция
// если в системе нескоько потоков обращаются к ресурсу, защищенному UnfairLock, доступ к ресурсу может получить произвольный поток, предпочтение будет отдаваться тому потоку, который обращается к ресурсу несколько раз, чтобы минимизировать context switch
class UnfairLockTest {

    private var lock = os_unfair_lock_s()

    func test() {
        os_unfair_lock_lock(&lock)
        //do something
        os_unfair_lock_unlock(&lock)
    }
}

//Дан код: Что выведется на экран?
// - Порядок может быть произвольным

/*private var lock = os_unfair_lock_s()

func test(i: Int) {
    os_unfair_lock_lock(&lock)
    sleep(1)
    print(i)
    os_unfair_lock_unlock(&lock)
}
let thread1 = Thread {
    test(i: 1)
}
thread1.start()

let thread2 = Thread {
    test(i: 2)
}
thread2.start()

let thread3 = Thread {
    test(i: 3)
}
thread3.start()*/

//// Synchronized

// в Swift мы не можем воспользоваться констурукций Synchronized напрямую
// когда в objc-c коде используется synchronized, компилятор преобразует ее в вызовы методов
// objc_sync_enter и objc_sync_exit
// эта конструкция основана на recursive mutex

class SynchronizedTest {

    // NSObject здесь используется в качестве мьютекса
    private var lock = NSObject()

    func test() {
        objc_sync_enter(lock)
        //do something
        objc_sync_exit(lock)
    }
}

class DeadLockTest {

    private let lock1 = NSLock()
    private let lock2 = NSLock()

    var resA = false
    var resB = false

    init() {

        let thread1 = Thread {

            self.lock1.lock()
            self.resA = true

            self.lock2.lock()
            self.resB = true
            self.lock2.unlock()

            self.lock1.unlock()
        }
        thread1.start()

        let thread2 = Thread {

            self.lock2.lock()
            self.resB    = true

            self.lock1.lock()
            self.resA = true
            self.lock1.unlock()

            self.lock2.unlock()
        }
        thread2.start()
    }
}




















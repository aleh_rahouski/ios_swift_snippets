//: Logger

/// Requirements

/* We will need to have multiple log levels. The log levels that our framework will
support are Fatal, Error, Warn, Debug, and Info.
We will need to have multiple logging profiles. The framework will define two profiles by default: LoggerNull and LoggerConsole. The LoggerNull profile will do nothing with the log message (it will pretty much ignore the message and not log it anywhere) while the LoggerConsole profile will print the log message to the console.
The user will have the ability to add their own logging profile so that they can log messages to a database, a UILabel, or any other location they want.
We must have the ability to configure the logging framework when the application starts and keep that configuration throughout the life cycle of the application. We do not want to force the users of our framework to reconfigure the framework every time they need to use it.
We can assign multiple logger profiles to a single log level to give the user the ability to display or store the logs to multiple profiles. */

import UIKit

protocol LoggerProfile {
    var loggerProfileId: String { get } //uniquely identify the logging profile
    func writeLog(level: String, message: String)
}

extension LoggerProfile {
    func getCurrentDateString() -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy hh:mm"
        return dateFormatter.string(from: date)
    }
}

struct LoggerNull : LoggerProfile {
    
    let loggerProfileId = "com.newfuturevision.logger.null"
    func writeLog(level: String, message: String) {
        //Do nothing
    }
}

struct LoggerConsole: LoggerProfile {
    
    let loggerProfileId = "com.newfuturevision.logger.console"
    func writeLog(level: String, message: String) {
        let now = getCurrentDateString()
        print("\(now): \(level) - \(message)")
    }
}

enum LogLevels: String {
    case fatal
    case error
    case warn
    case debug
    case info
    
    static let allValues = [fatal, error, warn, debug, info]
}

protocol Logger {
    static var loggers: [LogLevels: [LoggerProfile]] { get set }
    static func writeLog(logLevel: LogLevels, message: String)
}

extension Logger {
    
    //return `true` if the log level contains the logger profile
    static func logLevelContainsProfile(logLevel: LogLevels, loggerProfile: LoggerProfile) -> Bool {
        if let logProfiles = loggers[logLevel] {
            for logProfile in logProfiles where logProfile.loggerProfileId == loggerProfile.loggerProfileId {
                return true
            }
        }
        return false
    }
    
    static func setLogLevel(logLevel: LogLevels, loggerProfile: LoggerProfile) {
        
        if let _ = loggers[logLevel] {
            if !logLevelContainsProfile(logLevel: logLevel, loggerProfile: loggerProfile) {
                loggers[logLevel]?.append(loggerProfile)
            }
        }
        else {
            var profiles = [LoggerProfile]()
            profiles.append(loggerProfile)
            loggers[logLevel] = profiles
        }
    }
    
    static func addLogProfileToAllLevels(loggerProfile: LoggerProfile) {
        for logLevel in LogLevels.allValues {
            setLogLevel(logLevel: logLevel, loggerProfile: loggerProfile)
        }
    }
    
    static func removeLogProfileFromLevel(logLevel: LogLevels, loggerProfile: LoggerProfile) {
        if var logProfiles = loggers[logLevel] {
            if let index = logProfiles.index(where: { $0.loggerProfileId == loggerProfile.loggerProfileId }) {
                logProfiles.remove(at: index)
            }
            loggers[logLevel] = logProfiles
        }
    }
    
    static func removeLogProfileFromAllLevels(loggerProfile: LoggerProfile) {
        
        for level in LogLevels.allValues {
            removeLogProfileFromLevel(logLevel: level, loggerProfile: loggerProfile)
        }
    }
    
    //returns `true` if the log level contains any logger profiles
    static func hasLoggerForLevel(_ logLevel: LogLevels) -> Bool {
        guard let _ = loggers[logLevel] else {
            return false
        }
        return true
    }
}

struct MyLogger: Logger {
    static var loggers = [LogLevels : [LoggerProfile]]()
    
    static func writeLog(logLevel: LogLevels, message: String) {
        guard hasLoggerForLevel(logLevel) else {
            print("No Logger")
            return
        }
        if let logProfiles = loggers[logLevel] {
            for profile in logProfiles {
                profile.writeLog(level: logLevel, message: message)
            }
        }
    }
}

//Usage

MyLogger.addLogProfileToAllLevels(loggerProfile: LoggerConsole())
MyLogger.writeLog(logLevel: .debug, message: "debug message")
MyLogger.writeLog(logLevel: .error, message: "error message")










